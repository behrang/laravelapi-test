<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// routes for team
Route::get('teams', 'Api\TeamController@index');
Route::get('teams/{team}', 'Api\TeamController@show');
Route::post('teams', 'Api\TeamController@store');
Route::put('teams/{team}', 'Api\TeamController@update');
Route::delete('teams/{team}', 'Api\TeamController@delete');

// routes for player
Route::get('players', 'Api\PlayerController@index');
Route::get('players/{player}', 'Api\PlayerController@show');
Route::post('players', 'Api\PlayerController@store');
Route::put('players/{player}', 'Api\PlayerController@update');
Route::delete('players/{player}', 'Api\PlayerController@delete');

// routes for country
Route::get('countries', 'Api\CountryController@index');
