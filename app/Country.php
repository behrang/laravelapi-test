<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'name',
	];
	
	/**
	 * Get the teams for the country.
	 */
	public function teams()
	{
		return $this->hasMany('App\Team');
	}
}
