<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlayerController extends Controller
{
	/**
	 * Get all players
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\App\Players[]
	 */
	public function index()
	{
		return view('players');
	}
}
