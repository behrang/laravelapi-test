<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Player;

/**
 * 
 * @author 3ehrang
 * 
 * Api player controller
 *
 */
class PlayerController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		// secure api
        $this->middleware('auth:api', ['except' => array('index')]);
	}
	
	/**
	 * Get all players
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\App\Player[]
	 */
	public function index()
	{
		return Player::with('Team')->get();
	}
	
	/**
	 * Get Player
	 *
	 * @param Player $player
	 * @return \App\Player
	 */
	public function show(Player $player)
	{
		return $player;
	}
	
	/**
	 * Create new Player
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request)
	{
		$player = Player::create($request->all());
		return response()->json($player, 201);
	}
	
	/**
	 * Update Player
	 *
	 * @param Request $request
	 * @param Player $player
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(Request $request, Player $player)
	{
		$player->update($request->all());
		
		return response()->json($player, 200);
	}
	
	/**
	 * Delete Player
	 *
	 * @param Player $player
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete(Player $player)
	{
		$player->delete();
		
		return response()->json(null, 204);
	}
}
