<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;

/**
 * 
 * @author 3ehrang
 * 
 * Api team controller
 *
 */
class TeamController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		// secure api
        $this->middleware('auth:api', ['except' => array('index')]);
	}
	
	/**
	 * Get all teams
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\App\Team[]
	 */
	public function index()
	{
		return Team::with('Country')->get();
	}
	
	/**
	 * Get team
	 *
	 * @param Team $team
	 * @return \App\Team
	 */
	public function show(Team $team)
	{
		return $team;
	}
	
	/**
	 * Create new team
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request)
	{
		$team = Team::create($request->all());
		return response()->json($team, 201);
	}
	
	/**
	 * Update team
	 *
	 * @param Request $request
	 * @param Team $team
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function update(Request $request, Team $team)
	{
		$team->update($request->all());
		
		return response()->json($team, 200);
	}
	
	/**
	 * Delete team
	 *
	 * @param Team $team
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete(Team $team)
	{
		$team->delete();
		
		return response()->json(null, 204);
	}
}
