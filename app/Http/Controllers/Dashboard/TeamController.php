<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Dashboard controller to manage team
 */
class TeamController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
	
	/**
     * Show all teams
     */
    public function index ()
    {
        return view('dashboard.teams');
    }
 
}
