<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Dashboard controller to manage team
 */
class SettingController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
	
	/**
     * Setting for passport auth
     */
    public function index ()
    {
        return view('dashboard.setting');
    }
 
}
