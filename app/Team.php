<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'name',
			'description',
			'country_id'
	];
	
	/**
	 * Get the country for the team.
	 */
	public function country()
	{
		return $this->belongsTo('App\Country');
	}
	
	/**
	 * Get the players for the team.
	 */
	public function players()
	{
		return $this->hasMany('App\Player');
	}
}
