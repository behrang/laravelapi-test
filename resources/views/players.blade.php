@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Players</div>
				<div class="panel-body table-responsive">
					<players-index></players-index>
                </div>
                <div class="panel-body">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

