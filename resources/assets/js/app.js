
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

/**
 * Vue component for view and manage team, players
 */
Vue.component(
	'teams-index',
	require('./components/teams/Index.vue')
);
Vue.component(
	'players-index',
	require('./components/players/Index.vue')
);
Vue.component(
	'teams-dashboard',
	require('./components/teams/Dashboard.vue')
);

/**
 * Vue component for laravel passport package
 */
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);


var app = new Vue({
	  el: '#app',
	  data: {
	    message: 'Using Vue.js'
	  }
});
