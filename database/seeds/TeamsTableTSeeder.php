<?php

use Illuminate\Database\Seeder;

class TeamsTableTSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// add record
    	factory(App\Team::class, 6)->create();
    }
}
