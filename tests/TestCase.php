<?php

/**
 * Change original TestCase class to use migrations and seed the database before each test.
 * add the DatabaseMigrations trait, and then add an Artisan call on our setUp() method
 */

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;
    
    public function setUp()
    {
    	parent::setUp();
    	$this->artisan('migrate');
    	$this->seed();
    }
}
